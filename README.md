# Dockerized Symfony Application

Sample Symfony application with Dockerfiles.

The application was created using the [official setup guide](https://symfony.com/doc/current/setup.html).


## Building the application

```bash
docker build -t symfony-app:local .
docker build -f Dockerfile.nginx -t symfony-web:local --build-arg ASSET_IMAGE=symfony-app:local .
```

Development images:

```bash
docker build -t symfony-app:local-dev --build-arg APP_ENV=dev .
docker build -f Dockerfile.nginx -t symfony-web:local-dev --build-arg ASSET_IMAGE=symfony-app:local-dev .
```

See the [Production Docker Compose](https://gitlab.com/docker-symfony/docker-compose) setup for usage example.
